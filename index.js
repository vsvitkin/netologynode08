
/************************
Vasily Svitkin 2017
Netology NodeJS course
Home work 8
*/



var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

server.listen(5000, function () {
  console.log('Server started on port 5000');
});
app.use(express.static(__dirname + '/public'));
io.on('connection', function (socket) {

  socket.on('message', function (data) {
    socket.broadcast.emit('message', {
      username: socket.username,
      message: data
    });
  });

  socket.on('user', function (username) {
    socket.username = username;
    socket.emit('login');
    socket.broadcast.emit('joined', {username: socket.username});
  });

  socket.on('disconnect', function () {
      socket.broadcast.emit('user left', {username: socket.username });
  });
});
