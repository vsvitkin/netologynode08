$(function() {
var userName;
var connected=false;

//DOM elements:
var inputLogin = $('#inputLogin');
var inputMessage = $('#inputMessage');

var chatWindow = $('#chatDiv');
var messages = $('#messagesList');

inputLogin.focus();


function showLine (message, options) {
  messages.append($('<li>').text(message));
}

$(window).keydown(function (event) {
  inputMessage.focus();
  if (event.which === 13) {
    if (userName) {
      console.log('Sending message...')
      sendMessage();
    } else {
      console.log('Logging in...')
      login();
    }
  }
});

  var socket = io();

  function login () {
    userName = inputLogin.val().trim();
      $('#loginDiv').hide();
      chatWindow.show();
      inputMessage.focus();
      socket.emit('user', userName);
  }


  function sendMessage () {
    let message = inputMessage.val();

    if (message && connected) {
      inputMessage.val('');
      showLine('You: '+message);
      socket.emit('message', message);
    }
  }


  socket.on('login', function (data) {
    connected = true;
    showLine(`Welcome to chat, ${userName}.`);
  });

  socket.on('message', function (data) {
    showLine(data.username+': '+data.message);
  });

  socket.on('joined', function (data) {
    showLine('New user joined: '+data.username);
  });

  socket.on('disconnect', function () {
    showLine('Disconnected');
  });

  socket.on('reconnect', function () {
    showLine('Reconnected');
    if (userName) {
      socket.emit('user', userName);
    }
  });

  socket.on('reconnect_error', function () {
    showLine('Can not reconnect');
  });

});
